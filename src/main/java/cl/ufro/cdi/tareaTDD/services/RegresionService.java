package cl.ufro.cdi.tareaTDD.services;

import cl.ufro.cdi.tareaTDD.model.Punto;
import cl.ufro.cdi.tareaTDD.model.RegresionLineal;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegresionService {

    public String obtenerRegresion(List<Punto> puntos){
        return new RegresionLineal(puntos).obtenerFormula();
    }
}
