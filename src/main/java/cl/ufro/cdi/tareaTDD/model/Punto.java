package cl.ufro.cdi.tareaTDD.model;

import lombok.Data;

@Data
public class Punto {
    private double X;
    private double Y;

    public Punto(double x, double y) {
        X = x;
        Y = y;
    }
}