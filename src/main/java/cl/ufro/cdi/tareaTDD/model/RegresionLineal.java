package cl.ufro.cdi.tareaTDD.model;

import lombok.Data;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Data
public class RegresionLineal {

    private List<Punto> puntos;

    public RegresionLineal(List<Punto> puntos){
        this.puntos = puntos;
    }
    public String obtenerFormula(){
        float sum_x = 0;
        float sum_y = 0;
        float sum_x_por_y = 0;
        float sum_x_2 = 0;
        float n = puntos.size();
        for(Punto p : puntos){
            sum_x+=p.getX();
            sum_y+=p.getY();
            sum_x_por_y+=p.getX()*p.getY();
            sum_x_2+=p.getX()*p.getX();
        }
        float x_prom = sum_x/n;
        float y_prom = sum_y/n;

        float b1 = (sum_x_por_y - ((sum_x*sum_y)/n))/ (sum_x_2 - ((sum_x*sum_x)/n));
        float b0 = y_prom-x_prom*b1;

        DecimalFormat df = new DecimalFormat("##.###");
        df.setRoundingMode(RoundingMode.FLOOR);

        String formula = "y=" + df.format(b0) +"+" + df.format(b1) + "x";

        return formula;
    }
}