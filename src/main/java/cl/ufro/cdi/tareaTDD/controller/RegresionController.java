package cl.ufro.cdi.tareaTDD.controller;

import cl.ufro.cdi.tareaTDD.model.Punto;
import cl.ufro.cdi.tareaTDD.services.RegresionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/regresion")
public class RegresionController {

    @GetMapping("/")
    public @ResponseBody String regresionLineal(@RequestParam List<Punto> puntos){
        return new RegresionService().obtenerRegresion(puntos);
    }

}
