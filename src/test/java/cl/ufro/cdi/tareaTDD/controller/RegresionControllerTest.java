package cl.ufro.cdi.tareaTDD.controller;

import cl.ufro.cdi.tareaTDD.model.Punto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.*;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class RegresionControllerTest {

    @Autowired
    private RegresionController regresionController;

    private List<Punto> puntos;

    @BeforeEach
    void setUp() {
        puntos = new ArrayList<>();
        puntos.add(new Punto(0,0.692847));
        puntos.add(new Punto(1,0.224983));
        puntos.add(new Punto(2,0.574601));
        puntos.add(new Punto(3,0.624027));
        puntos.add(new Punto(4,0.097651));
        puntos.add(new Punto(5,0.071073));
        puntos.add(new Punto(6,0.133361));
        puntos.add(new Punto(7,0.206914));
        puntos.add(new Punto(8,0.961989));
        puntos.add(new Punto(9,0.438110));
        puntos.add(new Punto(10,0.059712));
        puntos.add(new Punto(11,0.912941));
        puntos.add(new Punto(12,0.894481));
        puntos.add(new Punto(13,0.521333));

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void regresionLineal() {
        assertThat(regresionController.regresionLineal(puntos)).isEqualTo("y=0.334+0.019x");
    }
}